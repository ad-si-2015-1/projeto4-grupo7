Para rodarmos a aplicação é muito simples, basta fazer o download dos arquivos
"Servidor.jar" e "Cliente.jar". Primeiro rodamos o servidor no terminal, com
o seguinte comando:

java -jar Servidor.jar

Feito isso, basta rodar dois clientes, que serão os dois jogadores:

java -jar Cliente.jar

Apenas isso, e fique a vontade para jogar!!!