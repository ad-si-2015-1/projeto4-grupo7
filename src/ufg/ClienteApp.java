package ufg;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class ClienteApp {

	public static void main(String[] args) {

		Cliente cliente = new Cliente();
		cliente.setNome(cliente.getResposta("Digite o seu nome: "));
		
		try {
			URL url = new URL("http://localhost:1200/servidor?wsdl");
			QName qname = new QName("http://ufg/", "ServidorService");
			Service ws = Service.create(url, qname);
			IServidor servidor = ws.getPort(IServidor.class);
			
			if(!servidor.conecta(cliente.getNome()).equals("ok"))
				cliente.terminarExecucao();
			
			String status = null;
			int statusCount = 0;
			String statusAux = null;
			
			while(!servidor.getPartidaFinalizada()){
				
				status = servidor.getStatus(cliente.getNome());
				
				statusAux = statusCount == 0 ? servidor.getStatus(cliente.getNome()) : statusAux;
				
				if(!status.startsWith("Aguard") && !status.startsWith("\nAguard")){
					
					//menu
					if(status.startsWith("Digite")){	
						int resp = cliente.getRespostaMenu(status);
						
						if(resp == 3)
							cliente.imprime("Sua pontuacao e: " + servidor.getSomaCartas(cliente.getNome()).toString() + "\n\n");
						else if(resp == 4)
							cliente.imprime(servidor.listarCartas(cliente.getNome()) + "\n\n");
						else if(resp > 4)
							cliente.imprime("Opcao invalida, digite uma correta" + "\n\n");
						else{
							servidor.menuResp(resp, cliente.getNome());
							servidor.setStatus(cliente.getNome(), "");
						}
					}
					
					else{
						if(statusCount == 0){
							cliente.imprime(status);
							statusCount++;
						}
						else if(!status.equals(statusAux)){
							cliente.imprime(status);
							statusCount = 0;
						}						
					}
				}
				
				else{
					cliente.imprime(status);
					servidor.setStatus(cliente.getNome(), "");
					Thread.sleep(500);
				}
			}
			
			cliente.imprime(servidor.getFinalJogo());
			cliente.terminarExecucao();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
