package ufg;

import javax.jws.WebService;

@WebService(endpointInterface="ufg.IServidor")
public class Servidor implements IServidor{

	private Jogador[] jogadores;
	// baralho da partida
	private Baralho baralho;
	// campeao da partida
	private Jogador campeao;
	// marcador fim de jogo
	private Boolean partidaFinalizada;

	//numero de jogadores que pararam e esperam por resultado
	private int jogadoresParados;
	//Quantidade de jogadores
	private int qtdJogadores;
	private String finalJogo;


	public Servidor(int qtdJogadores){

		this.qtdJogadores = qtdJogadores;
		this.baralho = new Baralho(1,true);
		this.partidaFinalizada = false;
		this.jogadores = new Jogador[getQtdJogadores()];
		this.finalJogo = "";
	}

	@Override
	public String conecta(String nome){
		
		try {

			if(jogadores[jogadores.length - 1] == null){

				Jogador jogador = new Jogador();
				jogador.setNome(nome);
				
				for(int i = 0; i < jogadores.length; i++){
					if(jogadores[i] == null){
						jogadores[i] = jogador;
						i = jogadores.length;
					}
				}
	
				enviarMsg(jogador, "Voce esta conectado " + jogador.getNome() + "!");
				
				if(jogadores[jogadores.length - 1] == null)
					enviarMsg(jogador, "Aguardando conexoes....");
				
				else{
					new Thread(new ServidorThread(this)).start();
				}
			}
			
			else{
				return "Conexao recusada: já existem jogadores suficientes, espera a proxima";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "ok";
		
	}

	//metodo que distribui cartas para os jogadores e verifica o ganhador
	public void distribuiCartas() throws InterruptedException{

		//contador de cartas/rodadas
		int k = 0;

		while(!partidaFinalizada && k <= 51){

			for(int i = 0; i < this.jogadores.length; i++){

				// a primeira rodada, apenas distribui uma carta a cada jogador
				if(k < jogadores.length){
					this.jogadores[i].addCartas(baralho.darProximaCarta());
					enviarMsg(jogadores[i], "Sua primeira carta e: " + jogadores[i].getMinhasCartas()[0]);
					k++;
				}

				//Se todos decidiram parar para receber o resultado no final do jogo, então o campeao e informado
				else if(this.jogadoresParados == jogadores.length){

					campeao = new Jogador();
					campeao.setSomaCartas(0);

					for(Jogador j : jogadores){	
						campeao =  campeao.getSomaCartas() <= j.getSomaCartas() && j.getSomaCartas() <= 21 ? j : campeao; 						
					}

					this.partidaFinalizada = true;
					i = this.jogadores.length;
					informaCampeao();

				}

				else{
					// a partir da segunda rodada, conferir se algum jogador esta com 21
					for(int j = 0; j < this.jogadores.length; j++){
						if(this.jogadores[j].getSomaCartas() == 21){
							this.campeao = this.jogadores[j];
							this.partidaFinalizada = true;
							j = this.jogadores.length;
						}

						//verifica se algum jogador estourou (passou de 21), nesse caso ele ja e um perdedor (esta fora da partida)
						else if(this.jogadores[j].getSomaCartas() > 21){
							this.finalJogo += "\n\n" + jogadores[j].getNome() + ", voce estourou a quantidade de pontos, voce esta fora!!!\n";
							campeao = j == 0 ? jogadores[1] : jogadores[0];
							this.partidaFinalizada = true;
							j = this.jogadores.length;
						}
					}

					if(!partidaFinalizada && !jogadores[i].getParouDeJogar()){

						menu(jogadores[i]);
						
						while(!jogadores[i].getRespondeu())
							Thread.sleep(1000);
						
						enviarMsg(jogadores[i], "\n\nAguarde enquanto os outros jogadores fazem sua jogada");
					}
					//Mostra o vencedor do jogo
					else if(partidaFinalizada){
						i = this.jogadores.length;
						informaCampeao();
					}
				}
			}
		}

	}

	/**
	 * Metodo que envia mensagens informando o vencedor do jogo.
	 * */
	public void informaCampeao(){

		this.finalJogo += "Parabens " + campeao.getNome() + ", voce ganhouuuuu! Total de pontos: " + campeao.getSomaCartas() + "\n";
		this.finalJogo += "O jogador(a) " + campeao.getNome() + " ganhou o jogo!!! Pontos do camepao: " + campeao.getSomaCartas() + "\n";
		this.finalJogo += "O jogo acabou!!!";
		System.out.println(finalJogo);
		fecharConexaoJogadores();

	}

	public void fecharConexaoJogadores(){

		for(Jogador j : jogadores)
			j = null;

	}

	public void menu(Jogador jogador){

		String opcoes = "Digite uma das opcoes!!\r\n\r\n"
				+ "1-Pegar Carta\r\n"
				+ "2-Parar e esperar ate o final com minha pontuacao\r\n"
				+ "3-Ver minha pontuacao\r\n"
				+ "4-Listar minhas cartas\r\n\r\n"
				+ "Digite o numero da opcao escolhida: ";
		jogador.setStatus(opcoes);
		jogador.setRespondeu(false);
	}
	
	public void menuResp(Integer resp, String nome){
		
		boolean loop = true;
		Jogador jogador = this.getJogadorByName(nome);
		
		while (loop) {

			switch (resp) {
			case 1:
				this.addCartas(jogador);
				loop = false;
				jogador.setStatus("Aguarde");
				jogador.setRespondeu(true);
				break;
			case 2:
				jogador.setParouDeJogar(true);
				this.jogadoresParados++;
				loop = false;
				jogador.setStatus("Aguarde");
				jogador.setRespondeu(true);
				break;
			default:
				enviarMsg(jogador, "Opcao invalida, digite uma correta" + "\n\n");
				break;
			}
		}
		
	}
	
	public Integer getSomaCartas(String nome){
		
		return this.getJogadorByName(nome).getSomaCartas();
		
	}

	public void addCartas(Jogador jogador){

		jogador.addCartas(baralho.darProximaCarta());
		enviarMsg(jogador, listarCartas(jogador.getNome()));
		enviarMsg(jogador, "Agora voce tem: " + jogador.getSomaCartas() + " pontos");

	}

	public String listarCartas(String nome){

		Jogador jogador = this.getJogadorByName(nome);
		
		String mensagem = "\nLista de cartas\n";

		for(Cartas carta : jogador.getMinhasCartas()){
			if(carta != null)
				mensagem += carta.toString() + "\n";

		}

		return mensagem;

	}

	public Integer lerMsgMenu(Jogador jogador, String msg){

		try {

			return 1;//jogador.getStubCliente().getRespostaMenu(msg);
			
		} catch (Exception e) {
			System.out.println("Erro na leitura da mensagem");
			return null;
		}
	}

	public Boolean enviarMsg(Jogador jogador, String msg){

		String msgEnvio = "\n";
		msgEnvio += msg;

		jogador.setStatus(msgEnvio);

		System.out.println(msgEnvio);

		return true;
	}

	/**
	 * Metodo que envia a mensagem para todos os jogadores
	 */
	public Boolean enviarMsg(String msg){

		String msgEnvio = "\nMensagem para todos os jogadores: ";
		msgEnvio += msg;

			for(Jogador jogador : jogadores){
			
				if(jogador != null){
					jogador.setStatus(msgEnvio);
				}
			}
			
		System.out.println(msgEnvio);

		return true;
	}
	
	public void apagaStatus(Jogador jogador){
		
		if(!getStatus(jogador.getNome()).startsWith("Digite"))
			setStatus(jogador.getNome(), "");
		
	}

	public int getJogadoresParados() {
		return jogadoresParados;
	}

	public void setJogadoresParados(int jogadoresParados) {
		this.jogadoresParados = jogadoresParados;
	}

	public int getQtdJogadores() {
		return qtdJogadores;
	}

	public void setQtdJogadores(int qtdJogadores) {
		this.qtdJogadores = qtdJogadores;
	}

	@Override
	public String getStatus(String nome) {
		// TODO Auto-generated method stub
		return this.getJogadorByName(nome).getStatus();
	}
	
	public void setStatus(String nome, String status){
		this.getJogadorByName(nome).setStatus(status);
	}
	
	@Override
	public Boolean getPartidaFinalizada() {
		return partidaFinalizada;
	}

	
	public Jogador getJogadorByName(String nome) {
		
		for(Jogador j : this.jogadores){
			if(j.getNome().equals(nome))
				return j;
		}
		return null;
	}

	public String getFinalJogo() {
		return finalJogo;
	}

	public void setFinalJogo(String finalJogo) {
		this.finalJogo = finalJogo;
	}

}
